/*
 * Copyright (c) 2001, 2003, 2008 Todd C. Miller <Todd.Miller@courtesan.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "config.h"

#ifndef lint
static const char rcsid[] = "$Id: priv_mktemp.c,v 1.10 2008/08/18 15:02:43 millert Exp $";
#endif /* not lint */

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#include <ctype.h>
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif /* HAVE_UNISTD_H */

#include <extern.h>

#define MKTEMP_FILE	1
#define MKTEMP_DIR	2

static int
_mktemp(path, mode)
	char *path;
	int mode;
{
	char *start, *cp;
	int fd, r;
	char *alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	if (*path == '\0') {
		errno = EINVAL;
		return(0);
	}

	for (cp = path; *cp; cp++)
		;
	do {
		cp--;
	} while (cp >= path && *cp == 'X');
	start = cp + 1;

	for (;;) {
		for (cp = start; *cp; cp++) {
			r = arc4random_uniform(26 + 26);
			*cp = alphabet[r];
		}

		switch (mode) {
		case MKTEMP_FILE:
			fd = open(path, O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
			if (fd != -1 || errno != EEXIST)
				return fd;
			break;
		case MKTEMP_DIR:
			if (mkdir(path, S_IRUSR|S_IWUSR|S_IXUSR) == 0)
				return(0);
			if (errno != EEXIST)
				return(-1);
			break;
		}
	}
	/*NOTREACHED*/
}

int
priv_mkstemp(path)
	char *path;
{
	return (_mktemp(path, MKTEMP_FILE));
}

char *
priv_mkdtemp(path)
	char *path;
{
	int error;

	error = _mktemp(path, MKTEMP_DIR);
	return(error ? NULL : path);
}
